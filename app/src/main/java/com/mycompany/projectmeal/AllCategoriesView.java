package com.mycompany.projectmeal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class shows all the different categories in the app and lets the user add its own categories from a button in actionbar.
 */
public class AllCategoriesView extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, IFragment {

    private static final String TAG3 = "emilie";
    private static final String CLASS = "WhatClass";
    private static final String CATEGORY_NAME = "categoryName";
    private static final String FROM_CLASS = "fromClass";
    private EditText newCategoryField;

    private ListView listView;
    private DBHelper dbHelper;
    private Cursor allCategoriesCursor;
    private SwipeRefreshLayout swipeLayout;
    private AllCategoriesAdapter adapter;
    private ContextualActionBarAdapter contextualAdapter;
    private Category category;
    private ArrayList<String> categoryNames;
    private boolean doesExist;
    private Category searchCategory;
    private Category categoryFound;
    private TextView categoryName;
    private ArrayList<Category> categories;
    private List<Integer> keyList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories_view);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        dbHelper = new DBHelper(this);
        listView = (ListView)findViewById(R.id.allCategories_view_listView);
        allCategoriesCursor = dbHelper.getAllCategoriesSorted();
        adapter = new AllCategoriesAdapter(this, allCategoriesCursor);
        listView.setAdapter(adapter);

        //setListAdapter(contextualAdapter)
        contextualAdapter = new ContextualActionBarAdapter(this, R.layout.list_in_category_row, R.id.allCategories_row_categoryText, dbHelper.getCategoriesSorted());

        //find the category name/names to be able to delete categories.
        categories = new ArrayList<>();
        categories = dbHelper.getCategoriesSorted();
        categoryName = (TextView) findViewById(R.id.category_name);

        Intent intent = getIntent();
        String correctCategory = intent.getStringExtra("categoryName");
        categoryFound = null;

        for (int i = 0; i < categories.size(); i++) {
            searchCategory = categories.get(i);
            break;
        }
        try {
            String name = categoryFound.getName();
            categoryName.setText(name);
        } catch(Exception e){

        }

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // getting values from selected ListItem
                        // Starting single contact activity
                        String categoryName = ((TextView) view.findViewById(R.id.allCategories_row_categoryText)).getText().toString();

                        // Starting single contact activity
                        Intent in = new Intent(getApplicationContext(), SpecificCategoryList.class);
                        in.putExtra(CATEGORY_NAME, categoryName);
                        startActivity(in);
                    }
                });

        /*//To change the color when selecting items in the listview (short click)
        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        //For the contextual action bar menu when deleting a category with longClick
        listView.setChoiceMode(listView.CHOICE_MODE_MULTIPLE_MODAL);

        //
        //Methods for the contextual action bar (CAB)
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            private int categoryNumber = 0;

            ArrayList<Category> selectedCategories = new ArrayList<Category>();

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                if (checked) {
                    categoryNumber++;
                    contextualAdapter.setNewSelectedCategory(position, checked);
                    selectedCategories.add(categories.get(position));

                } else {
                    categoryNumber--;
                    contextualAdapter.removeSelectedCategory(position);
                }
                mode.setTitle(categoryNumber + " selected");
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                categoryNumber = 0;
                getMenuInflater().inflate(R.menu.contextual_actionbar_menu, menu);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                keyList = new ArrayList<Integer>(contextualAdapter.getCurrentCheckedPosition());

                Collections.sort(keyList, new Comparator<Integer>() {
                    @Override
                    public int compare(Integer lhs, Integer rhs) {
                        return lhs.compareTo(rhs);
                    }
                });
                Collections.reverse(keyList);

                switch (item.getItemId()) {

                    case R.id.delete_category:
                        categoryNumber = 0;
                        for (Integer i : keyList) {
                            dbHelper.deleteCategoryByName(categories.get(i).getName());
                        }
                        contextualAdapter.notifyDataSetChanged();
                        contextualAdapter.clearSelectedCategory();
                        refreshList();
                        mode.finish();
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                contextualAdapter.clearSelectedCategory();

            }
        });

        //Method for the long click function
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setItemChecked(position, !contextualAdapter.isPositionChecked(position));
                view.setSelected(true);
                return false;
            }
        });

        //Sets the SwipeLayout to refresh AllCategoriesView
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_category);
        swipeLayout.setOnRefreshListener(this);


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (listView == null || listView.getChildCount() == 0) ?
                                0 : listView.getChildAt(0).getTop();
                swipeLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.allcategories_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // adding a new category, opens a pop-up window
        if(id==R.id.add_category_button){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.alert_category);
            alertDialogBuilder.setIcon(R.mipmap.round_munchie);

            final EditText et = new EditText(this);

            //Filter to 12 characters
            InputFilter[] filters = new InputFilter[1];
            filters[0] = new InputFilter.LengthFilter(12);
            et.setFilters(filters);

            // Capitalizes the first letter of input text
            et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

            // set prompts.xml to alertdialog builder
            alertDialogBuilder.setView(et);

            // set dialog message
            alertDialogBuilder.setCancelable(true).setPositiveButton(R.string.alert_spara, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    category = new Category(0, et.getText().toString(), R.drawable.munchie_ring);
                    categoryNames = dbHelper.getCategoryNames();
                    doesExist = false;

                    for (int i = 0; i < categoryNames.size(); i++) {

                        if (categoryNames.get(i).equals(category.getName())) {
                            Toast.makeText(getApplicationContext(), R.string.toast_mealname, Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            break;
                        } else {
                            if (categoryNames.get(i).equals(categoryNames.get(categoryNames.size() - 1))) {
                                dbHelper.addCategoryToDB(category);
                                dialog.dismiss();

                            }
                        }
                    }
                    refreshList();
                }

            });
            alertDialogBuilder.setNegativeButton(R.string.alert_avbryt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setInverseBackgroundForced(true);
            alertDialog.show();
            return true;
        }

        else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showCategories(View view) {
        Intent i = new Intent(this, AllCategoriesView.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void newMeal(View view) {
        Intent i = new Intent(this, SetInfoView.class);
        i.putExtra(FROM_CLASS,"fragment");
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void showMap(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(CLASS, "fragment");
        startActivity(i);
    }


    @Override
    public void goHome(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void tellAbout(View view) {
        Intent intent = new Intent(this,AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Refreshes the content of AllCategoriesView on swipe.
     */
    @Override
    public void onRefresh() {
        Intent i = new Intent(this, AllCategoriesView.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    /**
     * Updates the listView
     */
    public void refreshList() {
        categories = dbHelper.getCategoriesSorted();
        contextualAdapter.notifyDataSetChanged();
        onRefresh();
    }
}
