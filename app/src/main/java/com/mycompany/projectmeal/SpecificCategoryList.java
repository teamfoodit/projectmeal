package com.mycompany.projectmeal;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Activity with listView, for a specific category.
 */
public class SpecificCategoryList extends AppCompatActivity implements IFragment {

    private static final String CLASS = "WhatClass";
    private static final String MEAL_NAME = "mealName";
    private static final String FROM_CLASS = "fromClass";

    private DBHelper dbHelper;
    private Cursor specificMealsCursor;
    private String categoryName;
    private SpecificCategoryAdapter adapter;
    private ListView list;
    private Bundle extras;
    private Intent intent;
    private String name;
    private Typeface customFont2;
    private String fonthPath2;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_category_list);

        ImageView imageView = (ImageView)findViewById(R.id.startImage);
        ImageView imageView1 = (ImageView)findViewById(R.id.titleText);
        dbHelper = new DBHelper(this);

        if(dbHelper.getMealNames().size()==0){
            imageView.setVisibility(View.VISIBLE);
            imageView1.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
            imageView1.setVisibility(View.INVISIBLE);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                categoryName = null;
            } else {
                categoryName = extras.getString("categoryName");
            }
        } else {
            categoryName = (String) savedInstanceState.getSerializable("categoryName");
        }

        textView = (TextView) findViewById(R.id.category_name);

        //change Font
        fonthPath2 = "fonts/Caviar_Dreams_Bold.ttf";
        customFont2 = Typeface.createFromAsset(this.getAssets(), fonthPath2);
        textView.setTypeface(customFont2);
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textView.setText(categoryName);

        //Prints out the meals in specific category in listview
        list = (ListView) findViewById(R.id.specific_category_list);
        specificMealsCursor = dbHelper.getMealsInSpecificCategory(categoryName);
        adapter = new SpecificCategoryAdapter(this, specificMealsCursor);
        list.setAdapter(adapter);

        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // getting values from selected ListItem
                        name = ((TextView) view.findViewById(R.id.textName)).getText().toString();

                        // Starting single contact activity
                        Intent i = new Intent(getApplicationContext(), Info.class);
                        i.putExtra(MEAL_NAME, name);
                        startActivity(i);
                    }
                });

    }

    @Override
    public void showCategories(View view) {
        intent = new Intent(this, AllCategoriesView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void newMeal(View view) {
        intent = new Intent(this, SetInfoView.class);
        intent.putExtra(FROM_CLASS, "fragment");
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showMap(View view) {
        intent = new Intent(this, MapsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CLASS, "fragment");
        startActivity(intent);
    }

    @Override
    public void goHome(View view) {
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void tellAbout(View view) {
        intent = new Intent(this, AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
