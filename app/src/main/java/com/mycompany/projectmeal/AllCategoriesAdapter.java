package com.mycompany.projectmeal;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * This class gets data from database and inserts the info to each row in the list of categories.
 */

public class AllCategoriesAdapter extends CursorAdapter {

    public AllCategoriesAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.activity_all_categories_row, null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView nameView = (TextView) view.findViewById(R.id.allCategories_row_categoryText);
        TextView scoreView = (TextView) view.findViewById(R.id.allCategories_row_textView);
        ImageView imageView = (ImageView) view.findViewById(R.id.allCategories_row_imageView);

        String categoryName = cursor.getString(cursor.getColumnIndex("categoryName"));
        int imageId = cursor.getInt(cursor.getColumnIndex("categoryImage"));
        String averageScore = cursor.getString(cursor.getColumnIndex("categoryScore"));

        if(averageScore.equals("9.99")){
            averageScore = "10.0";
        }

        //change Font
        String FontPath = "fonts/ArchitectsDaughter.ttf";
        String FonthPath2 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), FontPath);
        Typeface customFont2 = Typeface.createFromAsset(context.getAssets(), FonthPath2);
        nameView.setTypeface(customFont2);
        scoreView.setTypeface(customFont);

        nameView.setText(categoryName);
        imageView.setImageResource(imageId);
        scoreView.setText(averageScore);
    }

}