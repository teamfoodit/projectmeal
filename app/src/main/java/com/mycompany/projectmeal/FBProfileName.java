package com.mycompany.projectmeal;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.Profile;

/**
 * A simple {@link Fragment} subclass.
 */
public class FBProfileName extends Fragment implements ICommunicator {

    private TextView profileName;

    public FBProfileName() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fb_profile_name, container, false);
    }

    @Override
    public void respond(Profile profile) {
        if(profile != null){
            String getProfileName = profile.getName();
            profileName.setText(getProfileName);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileName = (TextView)view.findViewById(R.id.fb_profile_name);
    }
}
