package com.mycompany.projectmeal;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * The DBHelper class represents our database. It will create the required tables to store the data we need.
 * It contains methods to update, delete, get data and create new data to store.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "DB";

    //Database info
    private static final String DATABASE_NAME = "MealProjectDB";
    private static final int DATABASE_VERSION = 1;

    //Tables
    private static final String TABLE_MEAL = "Meal";
    private static final String TABLE_CATEGORY = "Category";
    private static final String TABLE_POSITION = "Position";

    //Meal table columns
    private static final String KEY_MEAL_ID = "_id";
    private static final String KEY_MEAL_NAME = "mealName";
    private static final String KEY_MEAL_CATEGORY = "mealCategory";
    private static final String KEY_MEAL_SCORE = "mealScore";
    private static final String KEY_MEAL_HEALTH = "mealHealthy";
    private static final String KEY_MEAL_TASTE = "mealTaste";
    private static final String KEY_MEAL_DESCRIPTION = "mealDescription";
    private static final String KEY_MEAL_IMAGE_ID = "pictureID";
    private static final String KEY_MEAL_POSITION = "mealPosition";

    //Category table columns
    private static final String KEY_CATEGORY_ID = "_id";
    private static final String KEY_CATEGORY_NAME = "categoryName";
    private static final String KEY_CATEGORY_SCORE = "categoryScore";
    private static final String KEY_CATEGORY_IMAGE = "categoryImage";

    //Position table columns
    private static final String KEY_POSITION_POSITION = "positionPosition";
    private boolean firstStarted = false;
    private Cursor cursor;
    private ContentValues cvs;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
    * Creating the tables
    * Called when creating the database for the first time
    * (should not be called if a database with the same name already exists)
    * */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MEAL_TABLE = "CREATE TABLE " + TABLE_MEAL +
                " (" +
                KEY_MEAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_MEAL_NAME + " TEXT, " +
                KEY_MEAL_CATEGORY + " INTEGER, " +
                KEY_MEAL_SCORE + " TEXT, " +
                KEY_MEAL_HEALTH + " INTEGER, " +
                KEY_MEAL_TASTE + " INTEGER, " +
                KEY_MEAL_DESCRIPTION + " TEXT, " +
                KEY_MEAL_IMAGE_ID + " TEXT, " +
                KEY_MEAL_POSITION + " TEXT" +
                ");";

        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORY +
                " (" +
                KEY_CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_CATEGORY_NAME + " VARCHAR(255), " +
                KEY_CATEGORY_SCORE + " TEXT, " +
                KEY_CATEGORY_IMAGE + " INTEGER" +
                ");";

        String CREATE_POSITION_TABLE = "CREATE TABLE " + TABLE_POSITION +
                " (" +
                KEY_POSITION_POSITION + " BLOB" +
                ");";

        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_MEAL_TABLE);
        db.execSQL(CREATE_POSITION_TABLE);

        /*String insertInto = "INSERT INTO " + TABLE_MEAL + " ";
        insertInto += "(" + KEY_MEAL_NAME + "," + KEY_MEAL_CATEGORY + "," +
                KEY_MEAL_SCORE + "," + KEY_MEAL_HEALTH + "," +
                KEY_MEAL_TASTE + "," + KEY_MEAL_IMAGE_ID + "," +
                KEY_MEAL_POSITION + ")";
        insertInto += "VALUES (";*/

        firstStarted = true;
    }

    /**
     * Handles upgrades for the database. If the database already exists it will drop the old tables and create tables
     * again with the new upgrades.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop the old table if it exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);

        // Create tables again
        onCreate(db);
        db.close();

    }

    private String generateInsert(String name, int score) {

        String insertIntoCategory = "INSERT INTO " + TABLE_CATEGORY + "(" + "Fisk" + ", " + "0" + ")";

        String sql1 = insertIntoCategory + "\"" + name + "\"," + "\"" + score + ");";
        return insertIntoCategory;
    }

    /**
     * Adding 10 Categories to the Database (only use once)
     */
    public void addCategoryToDbStart() {
        SQLiteDatabase db = getWritableDatabase();
        cvs = new ContentValues();
        cvs.put(KEY_CATEGORY_NAME, "Fisk");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Nötkött");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Vegetariskt");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Efterrätt");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Sallad");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Fläskkött");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Fågel");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Paj");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Husmanskost");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);
        cvs.put(KEY_CATEGORY_NAME, "Frukost");
        cvs.put(KEY_CATEGORY_SCORE, 0.0);
        cvs.put(KEY_CATEGORY_IMAGE, R.drawable.munchie_ring);
        db.insert(TABLE_CATEGORY, null, cvs);

        db.close();
    }

    /**
     * Method to add new meals to the database
     * @param meal
     * @return id - the meals specific id
     */
    public long addMealToDB(com.mycompany.projectmeal.Meal meal) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cvs = new ContentValues();
        cvs.put(KEY_MEAL_NAME, meal.getName());
        cvs.put(KEY_MEAL_SCORE, meal.getScore());
        cvs.put(KEY_MEAL_HEALTH, meal.getHealth());
        cvs.put(KEY_MEAL_TASTE, meal.getTaste());
        cvs.put(KEY_MEAL_CATEGORY, meal.getCategoryName());
        cvs.put(KEY_MEAL_IMAGE_ID, meal.getImageId());
        cvs.put(KEY_MEAL_POSITION, meal.getPosition());
        cvs.put(KEY_MEAL_DESCRIPTION, meal.getDescription());

        //cvs.put(KEY_MEAL_DESCRIPTION, meal.getDescription());
        long id = db.insert(TABLE_MEAL, null, cvs);
        Log.d(TAG, "meal id: " + id);

        db.close();

        return id;
    }


    /**
     * Cursor for categories
     * @return getReadableDatabase
     */
    public Cursor getAllCategoriesSorted() {
        SQLiteDatabase db = getReadableDatabase();

        cursor = db.query(TABLE_CATEGORY, null, null, null, null, null, null, null);
        db.close();
        return getReadableDatabase().query("Category", null, null, null, null, null, "categoryScore DESC, categoryName ASC");

    }

    /**
     * Get all positions for meals from the map
     * @return positions
     */
    public ArrayList<String> getAllPos(){
        ArrayList<Meal> meals = new ArrayList<>();
        meals = getMeals();
        ArrayList<String> positions = new ArrayList<>();

        for (int i = 0; i<meals.size();i++){
            if(meals.get(i).getPosition()!=null) {
                positions.add(meals.get(i).getPosition());
            }
        }
        return positions;
    }


    /**
     * Cursor for meals in the specific category view. Will be accessed when first selecting a category
     * in the category view. In that specific view all meals with this category will show.
     * @param s
     * @return getReadableDatabase()
     */
    public Cursor getMealsInSpecificCategory(String s) {

        SQLiteDatabase db = getReadableDatabase();
        cursor = db.query(TABLE_MEAL, null, null, null, null, null, null);
        String sel = "mealCategory=\"" + s + "\"";
        db.close();
        return getReadableDatabase().query("Meal", null, sel, null, null, null, "mealScore DESC");
    }


    /**
     * Cursor for all meals
     * @return getReadableDatabase()
     */
    public Cursor getAllMeals() {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MEAL, null, null, null, null, null, null, null);
        db.close();
        return getReadableDatabase().query("Meal", null, null, null, null, null, "mealScore DESC");
    }


    /**
     * Method to add new categories in the AllCategoriesView
     * @param category
     * @return id - the specific category id when adding a new category
     */
    public long addCategoryToDB(Category category) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cvs = new ContentValues();
        cvs.put(KEY_CATEGORY_NAME, category.getName());
        cvs.put(KEY_CATEGORY_SCORE, category.getAverageScore());
        cvs.put(KEY_CATEGORY_IMAGE, category.getImageId());
        long id = db.insert(TABLE_CATEGORY, null, cvs);

        db.close();

        return id;
    }

    /**
     * Method used to update categories
     * Make it possible to change only ONE category
     *
     * @param categoryName
     * @param imageId
     */
    public void updateCategoryScore(String categoryName, int imageId) {
        Helper helper = new Helper();
        ArrayList<Meal> meals = getMeals();
        ArrayList<Meal> inCategoryMeals = new ArrayList<>();
        double score;

        if (meals.size()>0) {
            for (int i = 0; i < meals.size(); i++) {
                if (meals.get(i).getCategoryName().equals(categoryName)) {
                    inCategoryMeals.add(meals.get(i));
                }
            }
            score =  helper.categoryGrader(inCategoryMeals);
        } else {
            score =  0;

        }

        SQLiteDatabase db = getWritableDatabase();
        String strSQL = "UPDATE " +TABLE_CATEGORY+ " SET " +KEY_CATEGORY_SCORE +" = '" +score + "', " +KEY_CATEGORY_IMAGE +" = '"+imageId+ "' WHERE " +KEY_CATEGORY_NAME+ " = '" + categoryName +"'";
        db.execSQL(strSQL);
        db.close();

    }

    /**
     * Update Old CategoryScore after deleting a meal
     * @param categoryName
     * @param imageId
     * @param categoryScoreOld
     */
    public void updateCategoryScoreAfterEdit(String categoryName, int imageId, double categoryScoreOld){
        ArrayList<Meal> meals = getMeals();
        ArrayList<Meal> inCategoryMeals = new ArrayList<>();
        double totalSum = 0;

        if (meals.size()>0) {
            for (int i = 0; i < meals.size(); i++) {
                if (meals.get(i).getCategoryName().equals(categoryName)) {
                    inCategoryMeals.add(meals.get(i));
                }
            }
        }

        for (int i = 0; i<inCategoryMeals.size();i++){
            totalSum+= inCategoryMeals.get(i).getScore();
        }
        totalSum = totalSum - categoryScoreOld;

        if(totalSum !=0) {
            if(inCategoryMeals.size() == 0){
                totalSum = 0.0;
            } else {
                totalSum = totalSum/inCategoryMeals.size();
                Log.d(TAG, String.valueOf(totalSum));
            }
        }

        SQLiteDatabase db = getWritableDatabase();
        String strSQL = "UPDATE " +TABLE_CATEGORY+ " SET " +KEY_CATEGORY_SCORE +" = '" +totalSum + "', " +KEY_CATEGORY_IMAGE +" = '"+imageId+ "' WHERE " +KEY_CATEGORY_NAME+ " = '" + categoryName +"'";
        db.execSQL(strSQL);
        db.close();
    }

    /**
     * Method to update a meal
     * @param oldName
     * @param name
     * @param score
     * @param imageId
     * @param position
     * @param categoryName
     * @param health
     * @param taste
     * @param description
     */
    public void updateMeal(String oldName, String name, double score,String imageId,String position, String categoryName, int health, int taste, String description) {
        SQLiteDatabase db = getWritableDatabase();
        String strSQL = "UPDATE " +TABLE_MEAL+ " SET " +KEY_MEAL_SCORE+" = '" +score+"', ";
                strSQL += KEY_MEAL_IMAGE_ID +" = '"+imageId+"', "+KEY_MEAL_HEALTH +" = '" +health+"', ";
                strSQL+= KEY_MEAL_TASTE +" = '"+taste+"', ";
                strSQL+= KEY_MEAL_CATEGORY +" = '"+categoryName+"', "+KEY_MEAL_DESCRIPTION +" = '" +description+"', ";
                strSQL+= KEY_MEAL_POSITION +" = '"+position+"', ";
                strSQL+= KEY_MEAL_NAME +" = '"+name+"' ";
                strSQL += " WHERE " +KEY_MEAL_NAME+ " = '"+ oldName +"'";

        db.execSQL(strSQL);
        db.close();
    }


    /**
     * Delete specific meal by name
     * @param name
     */
    public void deleteMealByName(String name){
        SQLiteDatabase db = getWritableDatabase();
        String strSQL = "DELETE FROM "+TABLE_MEAL+" WHERE " +KEY_MEAL_NAME+ " = '"+ name +"'";
        db.execSQL(strSQL);
        db.close();
    }

    /**
     * Delete specific category by name
     * @param name
     */
    public void deleteCategoryByName(String name){
        SQLiteDatabase db = getWritableDatabase();
        String strSQL = "DELETE FROM "+TABLE_CATEGORY+" WHERE " +KEY_CATEGORY_NAME+ " = '"+ name +"'";
        db.execSQL(strSQL);
        db.close();
    }

    /**
     * ArrayList of meals
     * @return an ArrayList, Meal, with all Meals
     */
    public ArrayList<Meal> getMeals() {
        ArrayList<Meal> meals = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.query(TABLE_MEAL, null, null, null, null, null,"mealScore DESC, mealName ASC");

        if (c.moveToFirst()) {
            int nameColumn = c.getColumnIndex(KEY_MEAL_NAME);
            int scoreColumn = c.getColumnIndex(KEY_MEAL_SCORE);
            int healthColumn = c.getColumnIndex(KEY_MEAL_HEALTH);
            int tasteColumn = c.getColumnIndex(KEY_MEAL_TASTE);
            int imageIdColumn = c.getColumnIndex(KEY_MEAL_IMAGE_ID);
            int positionColumn = c.getColumnIndex(KEY_MEAL_POSITION);
            int categoryColumn = c.getColumnIndex(KEY_MEAL_CATEGORY);
            int descriptionColumn = c.getColumnIndex(KEY_MEAL_DESCRIPTION);

            String name;
            double score;
            int health;
            int taste;
            String imageId;
            String position;
            String categoryName;
            String description;

            do {
                name = c.getString(nameColumn);
                score = c.getDouble(scoreColumn);
                health = c.getInt(healthColumn);
                taste = c.getInt(tasteColumn);
                imageId = c.getString(imageIdColumn);
                position = c.getString(positionColumn);
                categoryName = c.getString(categoryColumn);
                description = c.getString(descriptionColumn);

                meals.add(new Meal(name, score, imageId, position, categoryName, health, taste, description));
            } while (c.moveToNext());
        }
        db.close();
        return meals;
    }

    /**
     * ArrayList of Categories
     * @return an ArrayList, Category, with all Categories from the DB
     */
    public ArrayList<Category> getCategoriesSorted() {
        ArrayList<Category> categories = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.query(TABLE_CATEGORY, null, null, null, null, null,"categoryScore DESC, categoryName ASC");

        if (c.moveToFirst()) {
            int nameColumn = c.getColumnIndex(KEY_CATEGORY_NAME);
            int scoreColumn = c.getColumnIndex(KEY_CATEGORY_SCORE);
            int imageColumn = c.getColumnIndex(KEY_CATEGORY_IMAGE);
            String name;
            double score;
            int imageId;
            do {
                name = c.getString(nameColumn);
                score = c.getDouble(scoreColumn);
                imageId = c.getInt(imageColumn);
                categories.add(new Category(score, name, imageId));
            } while (c.moveToNext());
        }
        db.close();
        return categories;
    }

    /**
     * Sorted arrayList of Categories.
     * @return allCategories in an ArrayList, sorted after letters
     */
    public ArrayList<Category> getCategoriesSortedByLetter() {
        ArrayList<Category> categories = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.query(TABLE_CATEGORY, null, null, null, null, null,"categoryName ASC");

        if (c.moveToFirst()) {
            int nameColumn = c.getColumnIndex(KEY_CATEGORY_NAME);
            int scoreColumn = c.getColumnIndex(KEY_CATEGORY_SCORE);
            int imageColumn = c.getColumnIndex(KEY_CATEGORY_IMAGE);
            String name;
            double score;
            int imageId;
            do {
                name = c.getString(nameColumn);
                score = c.getDouble(scoreColumn);
                imageId = c.getInt(imageColumn);
                categories.add(new Category(score, name, imageId));
            } while (c.moveToNext());
        }
        db.close();
        return categories;
    }

    /**
     * Arraylist of meal names
     * @return an ArrayList, String, with all MealNames from the DB
     */
    public ArrayList<String> getMealNames() {
        ArrayList<String> mealNames = new ArrayList<>();
        ArrayList<Meal> meals = getMeals();

        for (int i = 0; i < meals.size(); i++) {
            mealNames.add(meals.get(i).getName());
        }
        return mealNames;
    }

    /**
     * @return an ArrayList, String, with all CategoryNames from the DB
     */
    public ArrayList<String> getCategoryNames(){
        ArrayList<String> categoryNames = new ArrayList<>();
        ArrayList<Category> categoris = getCategoriesSorted();

        for (int i = 0; i < categoris.size(); i++) {
            categoryNames.add(categoris.get(i).getName());
        }

        return categoryNames;
    }

    /**
     *
     * @return the Meal with that name
     * @param mealName
     */
    public Meal getMeal(String mealName){
        ArrayList<Meal> meals = getMeals();
        Meal foundMeal = null;

        for (int i = 0; i<meals.size();i++){
            if(meals.get(i).getName().equals(mealName)){
                foundMeal = meals.get(i);
            }
        } return foundMeal;
    }

    /**
     * Get a specific category by name
     * @param name
     * @return
     */
    public Category getCategoryByName(String name){
        ArrayList<Category> categories = getCategoriesSorted();
        Category correctCategory = null;

        for (int i = 0; i <categories.size();i++){
            if(categories.get(i).getName().equals(name)){
                correctCategory = categories.get(i);
            }
        }

        return correctCategory;
    }

    /**
     * Get the meal from a position
     * @param position
     * @return
     */
    public Meal getMealfromPosition (String position) {
            Meal correctMeal = null;
            ArrayList<Meal> meals = getMeals();
            for (int i = 0; i < meals.size(); i++) {
                if (meals.get(i).getPosition()!= null) {
                    if (meals.get(i).getPosition().equals(position)) {
                        correctMeal = meals.get(i);

                    }
                }
            }
            return correctMeal;
    }

}