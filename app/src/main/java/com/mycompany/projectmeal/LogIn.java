package com.mycompany.projectmeal;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.Profile;

/**
 * Class to login with Facebook or login without Facebook
 */
public class LogIn extends AppCompatActivity implements ICommunicator {

    private TextView clickToOpen;
    private Intent intent;
    private FBProfileName fbProfileName;
    private FBProfilePicture fbProfilePicture;
    private FragmentManager managerProfileName;
    private FragmentManager managerProfilePicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        clickToOpen = (TextView)findViewById(R.id.use_without_fb_text);
    }

    /**
     * Let's the user use the app without logging in to facebook
     * @param view
     */
    public void whithoutFacebook(View view) {
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void respond(Profile profile) {
        if(profile != null){
            managerProfilePicture = getFragmentManager();
            managerProfileName = getFragmentManager();

            fbProfilePicture = (FBProfilePicture) managerProfilePicture.findFragmentById(R.id.fragment_profile_name);
            fbProfileName = (FBProfileName) managerProfileName.findFragmentById(R.id.fragment_profile_picture);

            fbProfilePicture.respond(profile);
            fbProfileName.respond(profile);
            clickToOpen.performClick();
        }
    }

}
