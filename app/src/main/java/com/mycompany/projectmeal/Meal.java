package com.mycompany.projectmeal;

/**
 * The Meal-class contains all the attributes to create a Meal.
 */
public class Meal {

    private String name;
    private double score;
    private String imageId;
    private String position;
    private String categoryName;
    private int health;
    private int taste;
    private String description;

    public Meal(String name, double score, String imageId, String position, String categoryName, int health, int taste,String description) {
        this.name = name;
        this.score = score;
        this.imageId = imageId;
        this.position = position;
        this.categoryName = categoryName;
        this.health = health;
        this.taste = taste;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getScore() {
        return score;
    }

    public String getImageId() {
        return imageId;
    }

    public int getHealth() {
        return health;
    }

    public int getTaste() {
        return taste;
    }
}