package com.mycompany.projectmeal;

/**
 * This class contains all the data for each category.
 */
public class Category {

    private String name;
    private double averageScore;
    private int imageId;


    public Category(double averageScore, String name, int imageId) {
        setAverageScore(averageScore);
        setName(name);
        setImageId(imageId);
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    /**
     * @returns the average score for a category.
     */

    public double getAverageScore() {
        return averageScore;
    }

    /**
     * @returns the name of the category.
     */
    public String getName() {
        return name;
    }

    /**
     * sets the average score of the category to double averageScore.
     */
    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    /**
     * sets the name of the category to string name.
     */
    public void setName(String name) {
        this.name = name;
    }

}


