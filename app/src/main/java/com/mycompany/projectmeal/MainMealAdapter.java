package com.mycompany.projectmeal;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.Image;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Transformation;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Gets data from database to mainactivity.
 */

public class MainMealAdapter extends CursorAdapter {

    private Bitmap bitmap;
    private Typeface customFont;
    private String fontPath;
    private int scaleFactor;
    private BitmapFactory.Options opt;
    private int wWidth;
    private int wHeight;
    private String imageId;
    private String mealScore;
    private String mealName;
    private ImageView imageView;
    private TextView scoreView;
    private TextView nameView;
    private View v;
    private LayoutInflater inflater;

    /**
     * Adapter for ListView in main
     * @param context
     * @param c
     */
    public MainMealAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_in_main_row, null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        nameView = (TextView) view.findViewById(R.id.textNameInList);
        scoreView = (TextView) view.findViewById(R.id.textScore);
        imageView = (ImageView) view.findViewById(R.id.mealIcon);

        mealName = cursor.getString(cursor.getColumnIndex("mealName"));
        mealScore = cursor.getString(cursor.getColumnIndex("mealScore"));
        imageId = cursor.getString(cursor.getColumnIndex("pictureID"));

        if(mealScore.equals("9.99")){
            mealScore = "10.0";
        }

        wHeight = 50;
        wWidth = 50;
        opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageId, opt);
        scaleFactor = Math.min(opt.outHeight / wHeight, opt.outWidth / wWidth);

        opt = new BitmapFactory.Options();
        opt.inSampleSize = scaleFactor;
        bitmap = BitmapFactory.decodeFile(imageId, opt);

        //change Font
        fontPath = context.getString(R.string.font1);
        customFont = Typeface.createFromAsset(context.getAssets(), fontPath);
        nameView.setTypeface(customFont);
        scoreView.setTypeface(customFont);

        nameView.setText(mealName);
        scoreView.setText(context.getString(R.string.grade) + mealScore);
        imageView.setImageBitmap(bitmap);
    }
}
