package com.mycompany.projectmeal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

/**
 * Contains all logic code that controls the info view.
 */
public class Info extends AppCompatActivity implements IFragment {

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final String TAG = "ebba";
    private static final String EBBA = "ebbatest";
    private static final String TAG3 = "emilie";
    private static final String CLASS = "WhatClass";
    private static final String FROM_CLASS = "fromClass";
    private static final String THE_IMAGE_ID = "theImageId";
    private static final String THE_MEAL_NAME = "theMealName";
    private static final String POSITION = "Position";
    private DBHelper dbHelper;
    private Cursor cursor;
    private MainMealAdapter mainMealAdapter;
    private int healthGrade;
    private int tasteGrade;
    private String position;
    private boolean positionIsNull;

    private ImageView healthStar1;
    private ImageView healthStar2;
    private ImageView healthStar3;
    private ImageView healthStar4;
    private ImageView healthStar5;
    private ImageView healthStar6;
    private ImageView healthStar7;
    private ImageView healthStar8;
    private ImageView healthStar9;
    private ImageView healthStar10;

    private ImageView tasteStar1;
    private ImageView tasteStar2;
    private ImageView tasteStar3;
    private ImageView tasteStar4;
    private ImageView tasteStar5;
    private ImageView tasteStar6;
    private ImageView tasteStar7;
    private ImageView tasteStar8;
    private ImageView tasteStar9;
    private ImageView tasteStar10;

    private ImageView imageView;
    private TextView descriptionView;
    private Meal foundMeal;
    private TextView mealName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        mainMealAdapter = new MainMealAdapter(this, cursor);
        dbHelper = new DBHelper(this);

        mealName = (TextView) findViewById(R.id.infoView_mealName);
        mealName.setPaintFlags(mealName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        imageView = (ImageView) findViewById(R.id.mealPic);
        descriptionView = (TextView) findViewById(R.id.infoview);

        TextView categoryName = (TextView) findViewById(R.id.category_insert);
        ImageView imageView = (ImageView) findViewById(R.id.mealPic);
        TextView descriptionView = (TextView) findViewById(R.id.infoview);
        TextView score = (TextView) findViewById(R.id.grade);
        TextView tasteView = (TextView) findViewById(R.id.tasteGrade);
        TextView scoreView = (TextView) findViewById(R.id.healthGrade);
        TextView mapButtonView = (TextView) findViewById(R.id.showOnMap);
        TextView averageScore = (TextView) findViewById(R.id.average_score);
        TextView noInfo = (TextView) findViewById(R.id.no_info_text);
        Button mapButton = (Button)findViewById(R.id.showOnMap);
        View divider = (View) findViewById(R.id.divider7);

        //change Font
        String FontPath = "fonts/ArchitectsDaughter.ttf";
        String FonthPath2 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface customFont = Typeface.createFromAsset(this.getAssets(), FontPath);
        Typeface customFont2 = Typeface.createFromAsset(this.getAssets(), FonthPath2);
        mealName.setTypeface(customFont2);
        categoryName.setTypeface(customFont);
        tasteView.setTypeface(customFont2);
        scoreView.setTypeface(customFont2);
        descriptionView.setTypeface(customFont);
        score.setTypeface(customFont);
        mapButtonView.setTypeface(customFont2);
        averageScore.setTypeface(customFont);

        healthStar1 = (ImageView) findViewById(R.id.healthStar1);
        healthStar2 = (ImageView) findViewById(R.id.healthStar2);
        healthStar3 = (ImageView) findViewById(R.id.healthStar3);
        healthStar4 = (ImageView) findViewById(R.id.healthStar4);
        healthStar5 = (ImageView) findViewById(R.id.healthStar5);
        healthStar6 = (ImageView) findViewById(R.id.healthStar6);
        healthStar7 = (ImageView) findViewById(R.id.healthStar7);
        healthStar8 = (ImageView) findViewById(R.id.healthStar8);
        healthStar9 = (ImageView) findViewById(R.id.healthStar9);
        healthStar10 = (ImageView) findViewById(R.id.healthStar10);

        tasteStar1 = (ImageView) findViewById(R.id.tasteStar1);
        tasteStar2 = (ImageView) findViewById(R.id.tasteStar2);
        tasteStar3 = (ImageView) findViewById(R.id.tasteStar3);
        tasteStar4 = (ImageView) findViewById(R.id.tasteStar4);
        tasteStar5 = (ImageView) findViewById(R.id.tasteStar5);
        tasteStar6 = (ImageView) findViewById(R.id.tasteStar6);
        tasteStar7 = (ImageView) findViewById(R.id.tasteStar7);
        tasteStar8 = (ImageView) findViewById(R.id.tasteStar8);
        tasteStar9 = (ImageView) findViewById(R.id.tasteStar9);
        tasteStar10 = (ImageView) findViewById(R.id.tasteStar10);

        ArrayList<Meal> meals = dbHelper.getMeals();

        Intent intent = getIntent();
        String correctName = intent.getStringExtra("mealName");
        foundMeal = null;
        Meal searchMeal;

        for (int i = 0; i < meals.size(); i++) {
            searchMeal = meals.get(i);
            if (searchMeal.getName().equals(correctName)) {
                foundMeal = meals.get(i);
                break;
            }
        }
        try {
            String name = foundMeal.getName();
            String category = (foundMeal.getCategoryName());
            String imageId = foundMeal.getImageId();
            String description = foundMeal.getDescription();
            String noInfoText;

            if (description == null) {
                noInfoText = getString(R.string.no_info);
            }
            else {
                noInfoText = "";
            }

            double scoreDouble = foundMeal.getScore();
            String scoreString = Double.toString(scoreDouble);
            if (scoreString.equals("9.99")){
                scoreString = "10.0";
            }

            mealName.setText(name);
            categoryName.setText(category);
            score.setText(scoreString);
            descriptionView.setText(description);
            noInfo.setText(noInfoText);

            tasteGrade = foundMeal.getTaste();
            healthGrade = foundMeal.getHealth();
            if(foundMeal.getPosition()!=null) {
                Log.d(TAG, foundMeal.getPosition());
                position = foundMeal.getPosition();
                mapButton.setVisibility(View.VISIBLE);
            } else {
                position = null;
                mapButton.setVisibility(View.INVISIBLE);
                divider.setVisibility(View.INVISIBLE);
            }


            if (position == null) {
                positionIsNull = true;
            }
            else {
                positionIsNull = false;
            }

            Bitmap bitmap;
            int wHeight = 350;
            int wWidth = 200;
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageId, opt);
            int scaleFactor = Math.min(opt.outHeight / wHeight, opt.outWidth / wWidth);

            opt = new BitmapFactory.Options();
            opt.inSampleSize = scaleFactor;
            bitmap = BitmapFactory.decodeFile(imageId, opt);
            imageView.setImageBitmap(bitmap);

        } catch (Exception e) {

        }

        switch (tasteGrade) {
            case 1:
                tasteStar1.setImageResource(R.drawable.star2);
                break;
            case 2:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                break;
            case 3:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                break;
            case 4:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                break;
            case 5:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                break;
            case 6:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                tasteStar6.setImageResource(R.drawable.star2);
                break;
            case 7:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                tasteStar6.setImageResource(R.drawable.star2);
                tasteStar7.setImageResource(R.drawable.star2);
                break;
            case 8:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                tasteStar6.setImageResource(R.drawable.star2);
                tasteStar7.setImageResource(R.drawable.star2);
                tasteStar8.setImageResource(R.drawable.star2);
                break;
            case 9:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                tasteStar6.setImageResource(R.drawable.star2);
                tasteStar7.setImageResource(R.drawable.star2);
                tasteStar8.setImageResource(R.drawable.star2);
                tasteStar9.setImageResource(R.drawable.star2);
                break;
            case 10:
                tasteStar1.setImageResource(R.drawable.star2);
                tasteStar2.setImageResource(R.drawable.star2);
                tasteStar3.setImageResource(R.drawable.star2);
                tasteStar4.setImageResource(R.drawable.star2);
                tasteStar5.setImageResource(R.drawable.star2);
                tasteStar6.setImageResource(R.drawable.star2);
                tasteStar7.setImageResource(R.drawable.star2);
                tasteStar8.setImageResource(R.drawable.star2);
                tasteStar9.setImageResource(R.drawable.star2);
                tasteStar10.setImageResource(R.drawable.star2);
                break;

        }
        switch (healthGrade) {
            case 1:
                healthStar1.setImageResource(R.drawable.star2);
                break;
            case 2:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                break;
            case 3:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                break;
            case 4:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                break;
            case 5:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                break;
            case 6:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                healthStar6.setImageResource(R.drawable.star2);
                break;
            case 7:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                healthStar6.setImageResource(R.drawable.star2);
                healthStar7.setImageResource(R.drawable.star2);
                break;
            case 8:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                healthStar6.setImageResource(R.drawable.star2);
                healthStar7.setImageResource(R.drawable.star2);
                healthStar8.setImageResource(R.drawable.star2);
                break;
            case 9:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                healthStar6.setImageResource(R.drawable.star2);
                healthStar7.setImageResource(R.drawable.star2);
                healthStar8.setImageResource(R.drawable.star2);
                healthStar9.setImageResource(R.drawable.star2);
                break;
            case 10:
                healthStar1.setImageResource(R.drawable.star2);
                healthStar2.setImageResource(R.drawable.star2);
                healthStar3.setImageResource(R.drawable.star2);
                healthStar4.setImageResource(R.drawable.star2);
                healthStar5.setImageResource(R.drawable.star2);
                healthStar6.setImageResource(R.drawable.star2);
                healthStar7.setImageResource(R.drawable.star2);
                healthStar8.setImageResource(R.drawable.star2);
                healthStar9.setImageResource(R.drawable.star2);
                healthStar10.setImageResource(R.drawable.star2);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.actionbar_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Pop-up window when clicking the delete button in the actionbar
        if (id == R.id.delete_meal) {

            AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(this);
            deleteBuilder.setTitle(R.string.alert_erase);
            deleteBuilder.setMessage(R.string.alert_erase_meal);

            deleteBuilder.setPositiveButton(R.string.alert_erase, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dbHelper.deleteMealByName(foundMeal.getName());
                    dbHelper.updateCategoryScore(foundMeal.getCategoryName(),dbHelper.getCategoryByName(foundMeal.getCategoryName()).getImageId());
                    Toast.makeText(getApplicationContext(), R.string.alert_erased, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                    mainMealAdapter.notifyDataSetChanged();
                    dbHelper.close();

                }
            })
                    .setNegativeButton(R.string.alert_avbryt, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

            AlertDialog dialog = deleteBuilder.create();
            dialog.show();
            return true;
        }

        //When selecting the edit meal button in actionbar
        else if (id == R.id.edit_meal) {
            Intent intent = new Intent(this,SetInfoView.class);
            intent.putExtra(FROM_CLASS,"infoClass");
            intent.putExtra(THE_IMAGE_ID,foundMeal.getImageId());
            intent.putExtra(THE_MEAL_NAME,foundMeal.getName());
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method starts up the map and add the position to the intent.
     *
     * @param view
     */
    public void showOnMap(View view) {
        if (!positionIsNull) {
            Log.d(TAG, position.toString());
            Intent i = new Intent(this, MapsActivity.class);
            i.putExtra(POSITION, position);
            i.putExtra(CLASS,"Info");
            startActivity(i);
        } else {
            Toast.makeText(getApplicationContext(), R.string.toast_not_saved, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void goHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    public void tellAbout(View view) {
        Intent intent = new Intent(this, AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showMap(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(CLASS, "fragment");
        startActivity(i);
    }

    @Override
    public void newMeal(View view) {
        Intent i = new Intent(this, SetInfoView.class);
        i.putExtra(FROM_CLASS, "fragment");
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void showCategories(View view) {
        Intent i = new Intent(this, AllCategoriesView.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
