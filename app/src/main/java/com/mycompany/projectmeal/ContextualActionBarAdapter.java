package com.mycompany.projectmeal;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * This class is the adapter for the contextual action bar menu.
 * It will appear if the user long clicks an item in the list of categories and meals in main.
 * There will be a choice to delete this item or items when the action bar appears.
 */
public class ContextualActionBarAdapter extends ArrayAdapter<String> {

    private HashMap<Integer, Boolean> contextualAdapter = new HashMap<Integer, Boolean>();

    public ContextualActionBarAdapter(Context context, int resource, int textViewResourceId, ArrayList objects) {
        super(context, resource, textViewResourceId, objects);
    }

    /**
     * Method to find the selected item
     * @param position
     * @param check
     */
    public void setNewSelectedCategory(int position, boolean check) {
        contextualAdapter.put(position, check);
        notifyDataSetChanged();
    }

    /**
     * Method to be able to remove the selection from an item in the list.
     * @param position
     */
    public void removeSelectedCategory(int position) {
        contextualAdapter.remove(position);
        notifyDataSetChanged();
    }

    /**
     * Method to clear the selected item/items in the category list when exiting the contextual mode
     */
    public void clearSelectedCategory() {
        contextualAdapter = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    /**
     * Method to check if the list item is checked or not.
     * @param position
     * @return true || false - depending on the checkValue
     */
    public boolean isPositionChecked(int position) {
        Boolean checkValue = contextualAdapter.get(position);
        if(checkValue == null) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * @return contextualAdapter.keySet()
     */
    public Set<Integer> getCurrentCheckedPosition() {
        return contextualAdapter.keySet();

    }
}
