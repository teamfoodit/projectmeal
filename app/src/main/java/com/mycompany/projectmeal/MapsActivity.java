package com.mycompany.projectmeal;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * A class showing users' current location or the location of a specific meal on Google Map.
 * Also showing a map view of all the meals' position.
 */
public class MapsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, IFragment {

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final String CLASS = "WhatClass";
    private static final String FROM_CLASS = "fromClass";
    private static final String IMAGE = "image";
    private static final String POSITION = "Position";

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String position;
    private DBHelper dbHelper;

    public static final String TAG = "ebba";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LatLng latLng;
    private double lati;
    private double lngi;
    private ArrayList<String> allPositions;
    private MarkerOptions pos;
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        setUpMapIfNeeded();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10*1000)
                .setFastestInterval(1 * 1000);

        Intent i = getIntent();
        String fromClass = i.getStringExtra(CLASS);

        if(fromClass.equals("Info")){
            position = i.getStringExtra(POSITION);

        } else {
            dbHelper = new DBHelper(this);
            allPositions = new ArrayList<>();
            allPositions = dbHelper.getAllPos();

        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }
    @Override
    protected void onPause(){
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
            mGoogleApiClient.disconnect();
        }

    }
    @Override
    protected void onResume(){
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());

        if (position != null) {
            final Marker myMarker;
            String[] latLng = position.split(",");
            double latitude = Double.parseDouble(latLng[0]);
            double longitude = Double.parseDouble(latLng[1]);
            LatLng correctPosition = new LatLng(latitude,longitude);
            dbHelper = new DBHelper(this);

            pos = new MarkerOptions()
                    .position(correctPosition)
                    .title(dbHelper.getMealfromPosition(position).getName());

            myMarker = mMap.addMarker(pos);
            myMarker.hideInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(correctPosition, 14));
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){

            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent i = new Intent(getApplicationContext(), Info.class);
                i.putExtra("mealName", marker.getTitle());
                startActivity(i);
            }
        });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    myMarker.showInfoWindow();
                    return true;
                }
            });
        }
        else {

            for (int i = 0; i < allPositions.size(); i++) {
                Marker marker;
                position = allPositions.get(i);
                if(allPositions.get(i)!=null) {
                    String[] latLng = position.split(",");
                    double latitude = Double.parseDouble(latLng[0]);
                    double longitude = Double.parseDouble(latLng[1]);
                    LatLng correctPosition = new LatLng(latitude, longitude);

                    if (position != null) {
                        pos = new MarkerOptions()
                                .position(correctPosition)
                                .title(dbHelper.getMealfromPosition(position).getName());


                        marker = mMap.addMarker(pos);
                        marker.hideInfoWindow();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(correctPosition, 12));
                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                Intent i = new Intent(getApplicationContext(), Info.class);
                                i.putExtra("mealName", marker.getTitle());
                                startActivity(i);
                            }
                        });
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                marker.showInfoWindow();

                                return true;
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionbar_home_button) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showCategories(View view) {
        Intent i = new Intent(this, AllCategoriesView.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void newMeal(View view) {
        Intent i = new Intent(this, SetInfoView.class);
        i.putExtra(FROM_CLASS, "fragment");
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void showMap(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(CLASS, "fragment");
        startActivity(i);
    }

    @Override
    public void goHome(View view) {
            Intent intent = new Intent(this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
    }

    public void tellAbout(View view) {
        Intent intent = new Intent(this,AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}

