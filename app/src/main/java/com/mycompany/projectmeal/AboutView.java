package com.mycompany.projectmeal;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.Profile;

/**
 * This class shows how many meals and categories the user has captured in the app.
 */
public class AboutView extends AppCompatActivity implements IFragment, ICommunicator {

    private static final String CLASS = "WhatClass";
    private static final String FROM_CLASS = "fromClass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_view);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        DBHelper dbHelper = new DBHelper(this);

        TextView numberMealsView = (TextView)findViewById(R.id.numberMeals);
        TextView numberCategoriesView = (TextView)findViewById(R.id.numberCategories);

        String standardTextMeal = numberMealsView.getText().toString();
        String standardTextCategories = numberCategoriesView.getText().toString();

        String numberOfMeals = String.valueOf(dbHelper.getMeals().size());
        String numberOfCategories = String.valueOf(dbHelper.getCategoriesSorted().size());

        String FontPath = "fonts/ArchitectsDaughter.ttf";
        Typeface customFont = Typeface.createFromAsset(this.getAssets(), FontPath);
        numberMealsView.setTypeface(customFont);
        numberCategoriesView.setTypeface(customFont);

        numberMealsView.setText(standardTextMeal +" "+ numberOfMeals);
        numberCategoriesView.setText(standardTextCategories +" "+ numberOfCategories);
    }

    @Override
    public void tellAbout(View view) {
        Intent intent = new Intent(this, AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showMap(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(CLASS, "fragment");
        startActivity(i);
    }

    @Override
    public void goHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void newMeal(View view) {
        Intent i = new Intent(this, SetInfoView.class);
        i.putExtra(FROM_CLASS, "fragment");
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void showCategories(View view) {
        Intent i = new Intent(this, AllCategoriesView.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void respond(Profile profile) {
        if (profile != null) {
            FragmentManager managerProfilePicture = getFragmentManager();
            FragmentManager managerProfileName = getFragmentManager();

            FBProfilePicture fbProfilePicture = (FBProfilePicture) managerProfilePicture.findFragmentById(R.id.fragment_profile_picture);
            FBProfileName fbProfileName = (FBProfileName) managerProfileName.findFragmentById(R.id.fragment_profile_name);

            fbProfilePicture.respond(profile);
            fbProfileName.respond(profile);
        }
    }
}