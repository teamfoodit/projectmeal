package com.mycompany.projectmeal;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Profile;

/**
 * A simple {@link Fragment} subclass.
 */
public class FBProfilePicture extends Fragment implements ICommunicator {

    private com.facebook.login.widget.ProfilePictureView profilePicture;

    public FBProfilePicture() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fb_profile_picture, container, false);
    }

    @Override
    public void respond(Profile profile) {
        if(profile != null){
            String profileID = profile.getId();
            profilePicture.setProfileId(profileID);
        } else{
            profilePicture.setProfileId("");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profilePicture = (com.facebook.login.widget.ProfilePictureView)view.findViewById(R.id.fb_profile_picture);
    }
}
