package com.mycompany.projectmeal;

import android.view.View;

/**
 * This interface contains all methods for the fragmentsmenu.
 */
public interface IFragment {

    /**
     * This method starts up the AllCategoriesView
     * @param view
     */
    public void showCategories(View view);


    /**
     * This method starts up the setInfoView
     * @param view
     */
    public void newMeal(View view);


    /**
     * This method starts up the map with location for all meals.
     * @param view
     */
    public void showMap(View view);


    /**
     * This method starts up the MainActiity
     * @param view
     */
    public void goHome (View view);


    /**
     * This method starts up the About view
     * @param view
     */
    public void tellAbout(View view);

}
