package com.mycompany.projectmeal;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Gets data from database to SpecificCategory
 */
public class SpecificCategoryAdapter extends CursorAdapter {

    public static final String PICTURE_ID = "pictureID";
    public static final String MEAL_SCORE = "mealScore";
    public static final String MEAL_NAME = "mealName";
    
    private Typeface customFont2;
    private Typeface customFont;
    private String fonthPath2;
    private String fontPath;
    private String mealScore;
    private String mealName;
    private Bitmap bitmap;
    private int scaleFactor;
    private BitmapFactory.Options opt;
    private int wWidth;
    private int wHeight;
    private String imageId;
    private ImageView imageView;
    private TextView scoreView;
    private TextView nameView;
    private View v;
    private LayoutInflater inflater;

    /**
     * Adapter for Specific Category listView
     * @param context
     * @param c
     */
    public SpecificCategoryAdapter(Context context, Cursor c) {
            super(context, c, 0);
        }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_in_category_row, null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        nameView = (TextView) view.findViewById(R.id.textName);
        scoreView = (TextView) view.findViewById(R.id.textScore);
        imageView = (ImageView) view.findViewById(R.id.mealIcon);

        imageId = cursor.getString(cursor.getColumnIndex(PICTURE_ID));
        wHeight = 50;
        wWidth = 50;

        opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageId, opt);

        scaleFactor = Math.min(opt.outHeight / wHeight, opt.outWidth / wWidth);

        opt = new BitmapFactory.Options();
        opt.inSampleSize = scaleFactor;
        bitmap = BitmapFactory.decodeFile(imageId, opt);

        mealName = cursor.getString(cursor.getColumnIndex(MEAL_NAME));
        mealScore = cursor.getString(cursor.getColumnIndex(MEAL_SCORE));

        if(mealScore.equals("9.99")){
            mealScore = "10.0";
        }

        fontPath = context.getString(R.string.font1);
        fonthPath2 = context.getString(R.string.fonts2);
        customFont = Typeface.createFromAsset(context.getAssets(), fontPath);
        customFont2 = Typeface.createFromAsset(context.getAssets(), fonthPath2);
        nameView.setTypeface(customFont2);
        scoreView.setTypeface(customFont);

        nameView.setText(mealName);
        scoreView.setText(context.getString(R.string.grade) + mealScore);
        imageView.setImageBitmap(bitmap);
    }
}
