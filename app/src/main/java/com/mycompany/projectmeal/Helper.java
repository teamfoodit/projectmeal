package com.mycompany.projectmeal;

import android.content.Intent;
import android.view.View;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * Class that contains methods to calculate average grade of meals and categories
 */
public class Helper {

    private double categoryGrade;

    /**
     *  Method that calculates the average grade of a meal.
     *  @return returns the average grade of the meal.
     */
    public double mealGrader (int taste, int health) {
        double sum = taste + health;
        double grade = sum/2;

        return grade;
    }

    /**
     *  Method that calculates the average grade of a meal category.
     *  @return returns average grade of the category.
     */
    public double categoryGrader (ArrayList<Meal> arrayList) {
        int l = arrayList.size();
        double sum = 0;
        for (int i=0; i<l; i++) {
            sum = sum + arrayList.get(i).getScore();
        }

        if(sum !=0) {
           categoryGrade = sum / l;
        }

        if(categoryGrade !=0) {
            return new BigDecimal(categoryGrade).setScale(2, RoundingMode.HALF_UP).doubleValue();
        } else {
            return 0.0;
        }

    }
}
