package com.mycompany.projectmeal;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.Profile;
import java.util.ArrayList;
import java.util.List;

/**
 * MainActivity, containing listView of all Meals
 */
public class MainActivity extends AppCompatActivity implements IFragment, ICommunicator {

    private static final String FROM_CLASS = "fromClass";
    private static final String MEAL_NAME = "mealName";
    public static final String MEAL_NAME1 = "mealName";
    private DBHelper dbHelper;
    private static final String CLASS = "WhatClass";
    private Cursor allMealsCursor;
    private FBProfilePicture fbProfilePicture;
    private FBProfileName fbProfileName;
    private ListView list;
    private MainMealAdapter adapter;
    private ContextualActionBarAdapter contextualAdapter;
    private Meal searchMeal;
    private Meal foundMeal;
    private TextView mealName;
    private ArrayList<Meal> meals;
    private List<Integer> keyList;
    private String correctMeal;
    private Intent intent;
    private FragmentManager managerProfilePicture;
    private FragmentManager managerProfileName;
    private AlertDialog.Builder loseBuild;
    private int id;
    private String name;
    private SharedPreferences.Editor editor;
    private boolean isFirstRun;
    private SharedPreferences wmbPreference;
    private TextView textView;

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        textView = (TextView) findViewById(R.id.textNameInList);

        try {
            String name = textView.getText().toString();
            b.putString("mealName", name);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        ImageView imageView = (ImageView)findViewById(R.id.startImage);
        ImageView imageView1 = (ImageView)findViewById(R.id.titleText);
        dbHelper = new DBHelper(this);

        if(dbHelper.getMealNames().size()==0){
            imageView.setVisibility(View.VISIBLE);
            imageView1.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
            imageView1.setVisibility(View.INVISIBLE);
        }

        contextualAdapter = new ContextualActionBarAdapter(this, R.layout.list_in_main_row, R.id.main_meal_list, dbHelper.getMeals());
        wmbPreference = PreferenceManager.getDefaultSharedPreferences(this);
        isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun) {
            dbHelper.addCategoryToDbStart();
            editor = wmbPreference.edit();
            editor.putBoolean("FIRSTRUN", false);
            editor.commit();
        }

        list = (ListView)findViewById(R.id.main_meal_list);
        allMealsCursor = dbHelper.getAllMeals();
        adapter = new MainMealAdapter(this, allMealsCursor);
        list.setAdapter(adapter);
        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // getting values from selected ListItem
                        name = ((TextView) view.findViewById(R.id.textNameInList)).getText().toString();

                        // Starting single contact activity
                        intent = new Intent(getApplicationContext(), Info.class);
                        intent.putExtra(MEAL_NAME, name);
                        startActivity(intent);
                    }
                });

        meals = new ArrayList<>();
        meals = dbHelper.getMeals();
        mealName = (TextView) findViewById(R.id.textNameInList);

        intent = getIntent();
        correctMeal = intent.getStringExtra(MEAL_NAME1);
        foundMeal = null;

        for (int i = 0; i < meals.size(); i++) {
            searchMeal = meals.get(i);
            break;
        }
        try {
            String name = foundMeal.getName();
            mealName.setText(name);
        } catch(Exception e){

        }

        //To change the color when selecting items in the listview (short click)
        list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //For the contextual action bar menu when deleting a category with longClick
        list.setChoiceMode(list.CHOICE_MODE_MULTIPLE_MODAL);

        //Methods for the contextual action bar (CAB)
        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            private int mealNumber = 0;
            ArrayList<Meal> selectedMeals = new ArrayList<>();

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                mealNumber = 0;
                if (checked) {
                    mealNumber++;
                    contextualAdapter.setNewSelectedCategory(position, checked);
                    selectedMeals.add(meals.get(position));
                } else {
                    mealNumber--;
                    contextualAdapter.removeSelectedCategory(position);
                }
                mode.setTitle(mealNumber + getString(R.string.selected));

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mealNumber = 0;
                getMenuInflater().inflate(R.menu.contextual_actionbar_menu, menu);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                keyList = new ArrayList<>(contextualAdapter.getCurrentCheckedPosition());


               /* Collections.sort(keyList, new Comparator<Integer>() {
                    @Override
                    public int compare(Integer lhs, Integer rhs) {
                        return lhs.compareTo(rhs);
                    }
                });
                Collections.reverse(keyList);
                */

                switch (item.getItemId()) {

                    case R.id.delete_category:
                        mealNumber = 0;
                        for (Integer i : keyList) {
                            dbHelper.deleteMealByName(meals.get(i).getName());
                            dbHelper.updateCategoryScore(meals.get(i).getCategoryName(),dbHelper.getCategoryByName(meals.get(i).getCategoryName()).getImageId());

                        }
                        contextualAdapter.notifyDataSetChanged();
                        contextualAdapter.clearSelectedCategory();

                        refreshList();
                        mode.finish();

                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        //Method for the long click function
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                list.setItemChecked(position, !contextualAdapter.isPositionChecked(position));
                view.setSelected(true);
                return false;
            }
        });
    }

    @Override
    public void showCategories(View view) {
        intent = new Intent(this, AllCategoriesView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void newMeal(View view) {
        intent = new Intent(this, SetInfoView.class);
        intent.putExtra(FROM_CLASS, "fragment");
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showMap(View view) {
        intent = new Intent(this, MapsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CLASS, "fragment");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        id = item.getItemId();

        if (id == R.id.home) {
            loseBuild = new AlertDialog.Builder(this);
            loseBuild.setTitle(R.string.alert_obs);
            loseBuild.setMessage(R.string.alert_cancel);
            loseBuild.setPositiveButton(R.string.alert_yes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MainActivity.this.finish();

                        }
                    });
            loseBuild.setNegativeButton(R.string.alert_no,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //raderat en rad pga ArrayList finns inte längre
                        }
                    });
            loseBuild.show();

            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void goHome(View view) {
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void tellAbout(View view) {
        intent = new Intent(this, AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void respond(Profile profile) {
        if (profile != null) {
            managerProfilePicture = getFragmentManager();
            managerProfileName = getFragmentManager();

            fbProfilePicture = (FBProfilePicture) managerProfilePicture.findFragmentById(R.id.fragment_profile_picture);
            fbProfileName = (FBProfileName) managerProfileName.findFragmentById(R.id.fragment_profile_name);

            fbProfilePicture.respond(profile);
            fbProfileName.respond(profile);
        }
    }

    /**
     * Updates the listView
     */
    public void refreshList() {
        meals = dbHelper.getMeals();
        contextualAdapter.notifyDataSetChanged();
        onRefresh();
    }

    /**
     * Refresh the view, updates the view
     */
    public void onRefresh() {
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
