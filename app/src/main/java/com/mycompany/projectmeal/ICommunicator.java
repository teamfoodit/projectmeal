package com.mycompany.projectmeal;

import com.facebook.*;

/**
 * This class communicates between different facebook fragments.
 */
public interface ICommunicator {
    public void respond(Profile profile);
}
