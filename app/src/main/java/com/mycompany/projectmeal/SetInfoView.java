package com.mycompany.projectmeal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;

/**
 * This class lets the user sets the required info to the specific photo taken.
 * The user inserts name, description, how tasty-, how healthy the meal is and
 * adds location to the meal.
 */
public class SetInfoView extends AppCompatActivity implements IFragment {

    private static final String TAG = "debug";
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final String CLASS = "WhatClass";
    private static final String FROM_CLASS = "fromClass";
    public static final String THE_MEAL_NAME = "theMealName";
    public static final String THE_IMAGE_ID = "theImageId";

    private static boolean enablePosition;
    private String categoryName;
    private int tasteRate;
    private int healthRate;
    private String resID;
    private Uri fileUri;
    private File img;
    private ArrayList<ImageView> tasteStars;
    private ArrayList<ImageView> healthStars;
    private Spinner spinner;

    private ImageView mealPhoto;
    private ImageView healthStar1;
    private ImageView healthStar2;
    private ImageView healthStar3;
    private ImageView healthStar4;
    private ImageView healthStar5;
    private ImageView healthStar6;
    private ImageView healthStar7;
    private ImageView healthStar8;
    private ImageView healthStar9;
    private ImageView healthStar10;

    private ImageView tasteStar1;
    private ImageView tasteStar2;
    private ImageView tasteStar3;
    private ImageView tasteStar4;
    private ImageView tasteStar5;
    private ImageView tasteStar6;
    private ImageView tasteStar7;
    private ImageView tasteStar8;
    private ImageView tasteStar9;
    private ImageView tasteStar10;

    private EditText nameField;
    private EditText descriptingField;

    private DBHelper dbHelper;
    private Helper helper;
    private SetInfoView setInfoView;
    private MapsActivity mapsActivity;
    private String id;
    private GPSTracker gps;
    private double latitude;
    private double longitude;
    private String description;
    private int taste;
    private int health;
    private double sum;
    private String imageId;
    private String position;
    private Intent intent;
    private String mealName;
    private boolean isHealthRated;
    private boolean isTasteRated;
    private boolean hasName;
    private double oldMealScore;
    private boolean doesExist;
    private boolean toLong;
    private ArrayList<String> mealNames;
    private String name;
    private Intent goToAllCategoriesView;
    private ArrayList<Category> categories;
    private String oldname;
    private int scaleFactor;
    private BitmapFactory.Options opt;
    private int wWidth;
    private int wHeight;
    private ImageView imageView;
    private Bitmap bitmap;
    private File projectDir;
    private File picDir;
    private File dir;
    private Intent goToAllMealsView;
    private int categoryImageDrawable;
    private EditText nameField1;
    private String oldClass;
    private Meal editedMeal;
    private ImageView button;
    private File photo;
    private ImageView button1;
    private String oldCategoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.activity_set_info_view);
        } catch(Exception e){
            Intent main = new Intent(this, MainActivity.class);
            startActivity(main);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        initializeInstanceVariables();

        dbHelper = new DBHelper(this);
        intent = getIntent();
        oldClass = intent.getStringExtra("fromClass");
        nameField1 = (EditText) findViewById(R.id.name);
        descriptingField = (EditText) findViewById(R.id.description);
        mealPhoto = (ImageView) findViewById(R.id.image_view_big);
        helper = new Helper();

        if (oldClass.equals("infoClass")) {
            button = (ImageView) findViewById(R.id.save_meal_button);
            button.setVisibility(View.INVISIBLE);
            mealName = intent.getStringExtra("theMealName");
            editedMeal = dbHelper.getMeal(mealName);
            imageId = intent.getStringExtra("theImageId");
            nameField1.setText(editedMeal.getName());
            descriptingField.setText(editedMeal.getDescription());
            bitmap = BitmapFactory.decodeFile(imageId);
            mealPhoto.setImageBitmap(bitmap);
            oldMealScore = editedMeal.getScore();
            oldCategoryName = editedMeal.getCategoryName();

        } else {

            button1 = (ImageView) findViewById(R.id.update);
            button1.setVisibility(View.INVISIBLE);
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            photo = null;
            oldMealScore = 0;

            try {
                photo = createFile();
                resID = String.valueOf(getResources().getIdentifier(photo.getName(), "photo", getPackageName()));
                id = photo.getAbsolutePath();
                if (id == null){
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

                fileUri = Uri.fromFile(photo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            } catch (Exception e) {

            }
            startActivityForResult(intent, CAMERA_REQUEST_CODE);

        }

        initializeReferences();
        addStarsToArrayLists();
        initalizeSpinner();

        enablePosition = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gps.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gps.canGetLocation = true;
            Log.d(TAG, "onResume");
        } else {
            gps.canGetLocation = false;
        }
    }

    /**
     * saves all the stars to the right imageview
     */
    private void initializeReferences() {

        healthStar1 = (ImageView) findViewById(R.id.healthStar1);
        healthStar2 = (ImageView) findViewById(R.id.healthStar2);
        healthStar3 = (ImageView) findViewById(R.id.healthStar3);
        healthStar4 = (ImageView) findViewById(R.id.healthStar4);
        healthStar5 = (ImageView) findViewById(R.id.healthStar5);
        healthStar6 = (ImageView) findViewById(R.id.healthStar6);
        healthStar7 = (ImageView) findViewById(R.id.healthStar7);
        healthStar8 = (ImageView) findViewById(R.id.healthStar8);
        healthStar9 = (ImageView) findViewById(R.id.healthStar9);
        healthStar10 = (ImageView) findViewById(R.id.healthStar10);

        tasteStar1 = (ImageView) findViewById(R.id.tasteStar1);
        tasteStar2 = (ImageView) findViewById(R.id.tasteStar2);
        tasteStar3 = (ImageView) findViewById(R.id.tasteStar3);
        tasteStar4 = (ImageView) findViewById(R.id.tasteStar4);
        tasteStar5 = (ImageView) findViewById(R.id.tasteStar5);
        tasteStar6 = (ImageView) findViewById(R.id.tasteStar6);
        tasteStar7 = (ImageView) findViewById(R.id.tasteStar7);
        tasteStar8 = (ImageView) findViewById(R.id.tasteStar8);
        tasteStar9 = (ImageView) findViewById(R.id.tasteStar9);
        tasteStar10 = (ImageView) findViewById(R.id.tasteStar10);

        nameField = (EditText) findViewById(R.id.name);
        descriptingField = (EditText) findViewById(R.id.description);
        mealPhoto = (ImageView) findViewById(R.id.image_view_big);
    }

    /**
     * This method adds all the stars to the arraylists of stars.
     */
    private void addStarsToArrayLists() {
        tasteStars = new ArrayList<ImageView>();
        healthStars = new ArrayList<ImageView>();

        healthStars.add(healthStar1);
        healthStars.add(healthStar2);
        healthStars.add(healthStar3);
        healthStars.add(healthStar4);
        healthStars.add(healthStar5);
        healthStars.add(healthStar6);
        healthStars.add(healthStar7);
        healthStars.add(healthStar8);
        healthStars.add(healthStar9);
        healthStars.add(healthStar10);

        tasteStars.add(tasteStar1);
        tasteStars.add(tasteStar2);
        tasteStars.add(tasteStar3);
        tasteStars.add(tasteStar4);
        tasteStars.add(tasteStar5);
        tasteStars.add(tasteStar6);
        tasteStars.add(tasteStar7);
        tasteStars.add(tasteStar8);
        tasteStars.add(tasteStar9);
        tasteStars.add(tasteStar10);
    }

    /**
     * This method initalizes the spinner of categories.
     */
    private void initalizeSpinner() {
        dbHelper = new DBHelper(this);
        ArrayList<Category> categories = dbHelper.getCategoriesSortedByLetter();

        String[] categoryNames = new String[categories.size()];

        for (int i = 0; i < categories.size(); i++) {
            categoryNames[i] = categories.get(i).getName();
        }
        spinner = (Spinner) findViewById(R.id.spinnerCategories);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, categoryNames);
        adapter_state
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter_state);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(

        ) {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryName = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void initializeInstanceVariables() {
        dbHelper = new DBHelper(this);
        helper = new Helper();
        setInfoView = new SetInfoView();
        mapsActivity = new MapsActivity();
        gps = new GPSTracker(SetInfoView.this);
    }


    /**
     * Returns a value of how tasty the meal is between 1-10.
     *
     * @param view a star image that represent a number.
     */
    public void tasteRateListener(View view) {
        for (int tasteStar = R.id.tasteStar1; tasteStar <= R.id.tasteStar10; tasteStar++) {
            if (view.getId() == tasteStar) {
                changeIfTasteClicked(view);
                tasteRate = ((tasteStar - R.id.tasteStar1) + 1);
            }
        }
    }

    /**
     * @param view Returns a value of how healthy the meal is between 1-10.
     * @param view a star image that represents a number.
     */
    public void healthRateListener(View view) {
        for (int healthstar = R.id.healthStar1; healthstar <= R.id.healthStar10; healthstar++) {
            if (view.getId() == healthstar) {
                changeIfHealthClicked(view);
                healthRate = ((healthstar - R.id.healthStar1) + 1);
            }
        }
    }


    private void changeIfTasteClicked(View view) {
        for (int i = 0; i < tasteStars.size(); i++) {
            if (tasteStars.get(i).getId() <= view.getId()) {
                try{
                    tasteStars.get(i).setImageResource(R.drawable.star2);
                } catch(Exception e){
                    changeIfTasteClicked(view);
                }
            } else {
                tasteStars.get(i).setImageResource(R.drawable.star1);
            }
        }
    }


    private void changeIfHealthClicked(View view) {
        for (int i = 0; i < healthStars.size(); i++) {
            if (healthStars.get(i).getId() <= view.getId()) {
                healthStars.get(i).setImageResource(R.drawable.star2);
            } else {
                healthStars.get(i).setImageResource(R.drawable.star1);
            }
        }
    }

    /**
     * A switch that enables the user to save the location of the meal.
     *
     * @param view a switch that is eather enabled or disabled
     * @return returns true or false
     */
    public Boolean enablePosition(View view) {

        if (enablePosition == false) {
            enablePosition = true;

            if (gps.canGetLocation) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                position = latitude + ", " + longitude;

                if (position.equals("0.0, 0.0")) {
                    Toast.makeText(SetInfoView.this, getString(R.string.toast_gps_position), Toast.LENGTH_SHORT).show();
                    position = null;
                    view.performClick();
                }
            } else {
                gps.showSettingsAlert();
                view.performClick();
                if (gps.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    gps.canGetLocation = true;
                }
            }

        } else if (enablePosition == true) {
            enablePosition = false;
        }

        return enablePosition;
    }

    /**
     * a button that takes all the information from the user and saves it
     *
     * @param view a button that lets the user press the button 'save meal'
     */
    public void saveMealButton(View view) {

        try {
            if (isReadyToSave()) {

                description = descriptingField.getText().toString();
                sum = helper.mealGrader(tasteRate, healthRate);
                if (sum == 10) {
                    sum = 9.99;
                }

                imageId = fileUri.getPath();
                position = null;


                if (enablePosition) {
                    if (gps.canGetLocation) {
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        position = latitude + ", " + longitude;

                        if (!isHealthRated && !isTasteRated) {
                            Toast.makeText(getApplicationContext(), R.string.toast_addgrade, Toast.LENGTH_SHORT).show();
                            if (!isHealthRated) {
                                Toast.makeText(getApplicationContext(), getString(R.string.toast_addhealth), Toast.LENGTH_SHORT).show();
                            } else if (!isTasteRated) {
                                Toast.makeText(getApplicationContext(), getString(R.string.toast_addtaste), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } else if (!enablePosition) {
                    position = null;
                }

                dbHelper.addMealToDB(new Meal(name, sum, imageId, position, categoryName, healthRate, tasteRate, description));
                categories = dbHelper.getCategoriesSorted();
                categoryImageDrawable = 0;
                for (int i = 0; i < categories.size(); i++) {
                    if (categories.get(i).getName().equals(categoryName)) {
                        categoryImageDrawable = categories.get(i).getImageId();
                        dbHelper.updateCategoryScore(categoryName, categoryImageDrawable);

                    }

                }
                Intent goToAllMealsView = new Intent(this, MainActivity.class);
                goToAllMealsView.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToAllMealsView);
                finish();
            }

        } catch (Exception e) {
            goToAllMealsView = new Intent(this, MainActivity.class);
            goToAllMealsView.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(goToAllMealsView);
            finish();

        }

    }

    /**
     * @throws IOException
     * @returns the image in created file
     */
    private File createFile() throws IOException {
        dir = getDirectory();
        img = new File(dir, "photo" + (new Date().getTime()) + ".jpg");
        return img;
    }

    /**
     * @throws IOException
     * @returns a new created file
     */
    private File getDirectory() throws IOException {
        picDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        projectDir = new File(picDir, "projectPics");
        if (!projectDir.exists()) {
            if (!projectDir.mkdirs()) {
            }
        }
        return projectDir;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                imageView = (ImageView) findViewById(R.id.image_view_big);

                wHeight = 300;
                wWidth = 300;

                opt = new BitmapFactory.Options();
                opt.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(fileUri.getPath(), opt);

                scaleFactor = Math.min(opt.outHeight / wHeight, opt.outWidth / wWidth);

                opt = new BitmapFactory.Options();
                opt.inSampleSize = scaleFactor;
                bitmap = BitmapFactory.decodeFile(fileUri.getPath(), opt);

                imageView.setImageBitmap(bitmap);

            }
        }
    }

    /**
     * Button that updates a specific meal in the DB
     *
     * @param view
     */
    public void updateMeal(View view) {

        if (isReadyToSave()) {
            description = descriptingField.getText().toString();
            taste = tasteRate;
            health = healthRate;
            sum = helper.mealGrader(tasteRate, healthRate);
            if (sum == 10){
                sum = 9.99;
            }
            imageId = intent.getStringExtra(THE_IMAGE_ID);
            position = null;
            oldname = intent.getStringExtra(THE_MEAL_NAME);

            if (enablePosition) {
                if (gps.canGetLocation) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    position = latitude + ", " + longitude;

                }
            } else if (!enablePosition) {
                position = null;
            }

            dbHelper.updateMeal(oldname, name, sum, imageId, position, categoryName, health, taste, description);

            ArrayList<Meal> meals = dbHelper.getMeals();
            for (int i = 0; i<meals.size();i++) {
                Log.d(TAG, "mealName= "+meals.get(i).getName()+" mealScore= "+meals.get(i).getScore());
            }

            categories = dbHelper.getCategoriesSorted();
            int categoryImageDrawable = 0;
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).getName().equals(categoryName)) {
                    categoryImageDrawable = categories.get(i).getImageId();
                    dbHelper.updateCategoryScoreAfterEdit(oldCategoryName,categoryImageDrawable,oldMealScore);
                    dbHelper.updateCategoryScore(categoryName,categoryImageDrawable);
                    Log.d(TAG, "Score = " + String.valueOf(categories.get(i).getAverageScore()));
                }
            }

            for (int i = 0; i<categories.size();i++){
                Log.d(TAG,"Name= "+categories.get(i).getName()+"Score= "+categories.get(i).getAverageScore());
            }

            goToAllCategoriesView = new Intent(this, MainActivity.class);
            startActivity(goToAllCategoriesView);
            finish();
        }
    }

    @Override
    public void tellAbout(View view) {
        intent = new Intent(this, AboutView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showCategories(View view) {
        intent = new Intent(this, AllCategoriesView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void newMeal(View view) {
        intent = new Intent(this, SetInfoView.class);
        intent.putExtra(FROM_CLASS, "fragment");
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showMap(View view) {
        intent = new Intent(this, MapsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CLASS, "fragment");
        startActivity(intent);
    }

    @Override
    public void goHome(View view) {

        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    /**
     * Checks if all the fields are correctly filled, prints Toast for uncorrect fields
     *
     * @return true if everything is ok to save in DB
     */
    public boolean isReadyToSave() {
        name = nameField.getText().toString();
        description = descriptingField.getText().toString();
        mealNames = dbHelper.getMealNames();
        doesExist = false;

        if (name.equals("")) {
            hasName = false;
            Toast.makeText(getApplicationContext(), getString(R.string.need_name), Toast.LENGTH_SHORT).show();
        } else {
            hasName = true;
        }

        if (description.equals("")) {
            description = " ";
        }

        if (healthRate < 1) {
            Toast.makeText(getApplicationContext(), getString(R.string.need_grade), Toast.LENGTH_SHORT).show();
            isHealthRated = false;
        } else {
            isHealthRated = true;
        }
        if (tasteRate < 1) {
            Toast.makeText(getApplicationContext(), getString(R.string.need_grade), Toast.LENGTH_SHORT).show();
            isTasteRated = false;
        } else {
            isTasteRated = true;
        }

        if (name.length() > 12) {
            toLong = true;
            Toast.makeText(getApplicationContext(), R.string.to_long_name, Toast.LENGTH_SHORT).show();
        } else {
            toLong = false;
        }

        for (int i = 0; i < mealNames.size(); i++) {

            if (mealNames.get(i).equals(name)) {
                if (mealNames.get(i).equals(mealName)) {
                    doesExist = false;
                } else {
                    doesExist = true;
                    Toast.makeText(getApplicationContext(), R.string.toast_mealname, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        } return (!doesExist && isTasteRated && isHealthRated && !toLong && hasName);
    }
}